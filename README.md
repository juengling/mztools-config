# MZ-Tools-Einstellungen

## Beschreibung
Dieses Repository speichert meine persönlichen Einstellungen für die MZ-Tools. Der Zweck ist, dieselben Einstellungen auf unterschiedlichen Rechnern gleichermaßen verwenden zu können.

Siehe dazu auch [diesen Beitrag auf meiner Homepage](https://www.juengling-edv.de/projektuebergreifend-effizient-arbeiten/).

## Installation
Erstelle einen Clone dieses Projektes in deinem eigenen GitLab-Account. Dadurch hast du
wie ich die Möglichkeit, deine eigenen Einstellungen in der Cloud zu sichern. Wenn du das
nicht willst, kannst du diesen Schritt übergehen.

Beende zunächst alle Programme, die das MZ-Tools-Plugin benutzen.

Lösche dann alle Einstellungen von MZ-Tools unter deinem User, indem du alle Dateien aus dem Verzeichnis  

    %appdata%\MZTools Software\MZTools8\

entfernst. Falls du die Lizenzdatei ebenfalls in dem Verzeichnis liegen hast, lösche diese nicht, sondern verschiebe sie ggf. eine Ebene höher. Du wirst sie beim ersten Start der MZ-Tools unter Umständen wieder brauchen. 

Nun clone das Git-Repository (meins oder dein eigenes, siehe oben) direkt in das obige Verzeichnis.
Entferne den automatisch hinzugefügten letzten Teil des Pfades (das ist der Projektname), da sonst
ein Unterverzeichnis angelegt wird, wodurch MZ-Tools die Einstellungen nicht mehr findet.

Nun ist automatisch der **master**-Branch ausgecheckt.

Öffne deine Office-Applikation, aktiviere den VB-Editor und gehe dort zu den MZ-Tools-Optionen.
Ändere in den *Persönlichen Einstellungen* den Benutzernamen in deinen eigenen. Überprüfe eventuell
alle anderen Einstellungen.

Abschließend committe diese Änderung und pushe sie auf dein eigenes Repository. Von nun an brauchst du nach einem Clone auch deine eigenen Daten nicht mehr einzugeben.